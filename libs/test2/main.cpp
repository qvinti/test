#include <iostream>
#include <Windows.h>
using namespace std;

int main()
{
	setlocale(LC_ALL, "ru");
	const double exp = 2.71, n = 10, m = 5, x = 0.9;
	int is, ie, js, je;
	cout << "is = ";
	cin >> is;
	cout << "ie = ";
	cin >> ie;
	cout << "js = ";
	cin >> js;
	cout << "je = ";
	cin >> je;
	double s;
	HINSTANCE load;
	load = LoadLibrary(L"DynamicLib.dll");
	typedef double (*mul) (int, int);
	mul Mul;
	Mul = (mul)GetProcAddress(load, "Mul");
	typedef double (*sum) (int, int, const double);
	sum Sum;
	Sum = (sum)GetProcAddress(load, "Sum");
	s = pow(exp, -2 * x) * Sum(is, ie, n) + Mul(js, je);
	printf("s=%7.5f\n", s);
}