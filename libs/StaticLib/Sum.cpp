#include <math.h>
#include <cmath>
double sum(int fs, int fe, const double fn)
{
	double sm = 0;
	int i;
	i = fs - 1;
	do
	{
		i++;
		sm = sm + fn * abs(log10(i));
	} while (i <= fe);
	return sm;
}