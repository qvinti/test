#include <cmath>
extern "C" __declspec(dllexport) double mul(int fs, int fe)
{
	double ml = 1;
	int i;
	i = fs - 1;
	do
	{
		i++;
		ml = ml * pow(cos(i), 2);
	} while (i <= fe);
	return ml;
}